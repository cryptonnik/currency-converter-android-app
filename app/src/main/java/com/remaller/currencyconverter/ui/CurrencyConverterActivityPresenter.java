package com.remaller.currencyconverter.ui;

import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.ExchangeRatesService;

import org.joda.time.DateTime;

import java.util.Currency;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class CurrencyConverterActivityPresenter {

	public interface PresenterView {

		void showNoRatesViewState();
		void setCurrencyValues(List<CurrencyListAdapter.Item> items, CurrencyRatesState.Error error);
		void moveItemToTop(int itemPosition);
		void focusFirstItem();
		void hideKeyboard();
	}

	private final CurrencyRatesStateController currencyRatesStateController;
	private final ExchangeRatesService exchangeRatesService;
	private final CurrencyAmountCalculator amountCalculator;
	private final CurrencyAmountFormatter amountFormatter;
	private final CurrencyOrderController currencyOrderController;
	private final CurrencyItemsBuilder currencyItemsBuilder;

	private final CompositeDisposable disposables = new CompositeDisposable();

	private PresenterView view;

	private DateTime ratesUpdateTime = new DateTime(0);

	private String topCurrency;
	private Double topCurrencyAmountValue = null;
	private String topCurrencyAmountCustomText = null;
	private CurrencyRatesState.Error error;
	private List<String> initialCurrencyOrder;

	public CurrencyConverterActivityPresenter(
			CurrencyRatesStateController currencyRatesStateController,
			ExchangeRatesService exchangeRatesService,
			CurrencyAmountCalculator amountCalculator,
			CurrencyAmountFormatter amountFormatter,
			CurrencyOrderController currencyOrderController,
			CurrencyItemsBuilder currencyItemsBuilder
	) {
		this.currencyRatesStateController = currencyRatesStateController;
		this.exchangeRatesService = exchangeRatesService;
		this.currencyOrderController = currencyOrderController;
		this.amountCalculator = amountCalculator;
		this.currencyItemsBuilder = currencyItemsBuilder;
		this.amountFormatter = amountFormatter;
	}

	public void onCreate(PresenterView view) {

		this.view = view;

		Disposable disposable1 =
				currencyRatesStateController.getRatesStateChangedObservable()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(exchangeRatesState -> updateValues());

		Disposable disposable2 =
				exchangeRatesService.loadCurrencyOrderState()
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe((currencyOrderState, throwable) -> {

						if(throwable != null || currencyOrderState == null) {

							topCurrencyAmountValue = 100.0;

						} else {

							initialCurrencyOrder = currencyOrderState.order;
							topCurrencyAmountValue = (double) currencyOrderState.firstAmount;
						}

						updateValues();
					});

		disposables.add(disposable1);
		disposables.add(disposable2);

		updateValues();
	}

	public void onDestroy() {
		disposables.dispose();
	}

	public void onStart() {
		exchangeRatesService.startUpdating();
	}

	public void onStop() {
		exchangeRatesService.stopUpdating();
	}

	public void onPause() {

		if(topCurrencyAmountValue == null)
			return;

		exchangeRatesService.saveCurrencyOrderState(
				currencyOrderController.getCurrencyOrder(), topCurrencyAmountValue.floatValue());
	}

	public void onItemClicked(String currency) {

		int pos = currencyOrderController.getCurrencyPos(currency);

		if(pos == -1)
			return;

		if(pos == 0) {

			view.focusFirstItem();
			return;
		}

		topCurrencyAmountValue =
				amountFormatter.roundAmount(
						amountCalculator.calculateAmountInNewCurrency(topCurrencyAmountValue, topCurrency, currency),
						Currency.getInstance(currency));

		topCurrency = currency;
		topCurrencyAmountCustomText = null;

		currencyOrderController.moveCurrencyToTop(currency);

		view.moveItemToTop(pos);
		view.focusFirstItem();
		updateCurrencyList();
	}

	public void onFirstItemValueChanged(String value) {

		if(value.isEmpty())
			topCurrencyAmountValue = null;
		else
			topCurrencyAmountValue = amountFormatter.parseAmount(value);

		topCurrencyAmountCustomText = value;
		updateCurrencyList();
	}

	public void onFirstItemVisibilityChanged(boolean visible) {

		if(!visible)
			view.hideKeyboard();
	}

	// =============================================================================================

	private void updateValues() {

		CurrencyRatesState state = currencyRatesStateController.getRatesState();

		error = state.error;

		if(state.rates == null || state.rates.isEmpty()) {

			view.showNoRatesViewState();
			return;
		}

		if(state.updateTime != null && !state.updateTime.equals(ratesUpdateTime)) {

			amountCalculator.updateRates(state.baseCurrency, state.rates);
			currencyOrderController.setCurrencies(state.baseCurrency, state.rates.keySet());
		}

		if(initialCurrencyOrder != null) {

			currencyOrderController.applyCurrencyOrder(initialCurrencyOrder);
			initialCurrencyOrder = null;
		}

		updateCurrencyList();
	}

	private void updateCurrencyList() {

		String topCurrency = currencyOrderController.getCurrencyByPos(0);

		if(topCurrency == null)
			return;

		if(!topCurrency.equals(this.topCurrency))
			this.topCurrency = topCurrency;

		List<CurrencyListAdapter.Item> items =
				currencyItemsBuilder.createCurrencyItems(
						topCurrency,
						topCurrencyAmountValue,
						topCurrencyAmountCustomText,
						currencyOrderController.getCurrencyOrder(),
						amountCalculator,
						amountFormatter);

		view.setCurrencyValues(items, error);
	}
}
