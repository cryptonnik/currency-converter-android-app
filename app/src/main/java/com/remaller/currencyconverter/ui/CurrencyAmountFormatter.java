package com.remaller.currencyconverter.ui;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;

public class CurrencyAmountFormatter {

	public String format(double amount, Currency currency) {

		NumberFormat numberFormat = NumberFormat.getInstance();

		numberFormat.setGroupingUsed(false);

		numberFormat.setMinimumFractionDigits(currency.getDefaultFractionDigits());
		numberFormat.setMaximumFractionDigits(currency.getDefaultFractionDigits());

		return numberFormat.format(amount);
	}

	public double roundAmount(double amount, Currency currency) {

		double coef = Math.pow(10, currency.getDefaultFractionDigits());
		return Math.round(amount * coef) / coef;
	}

	public double parseAmount(String value) {

		NumberFormat numberFormat = NumberFormat.getInstance();

		try {
			return numberFormat.parse(value).doubleValue();

		} catch (ParseException e) {
			return 0.0;
		}
	}
}
