package com.remaller.currencyconverter.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class SoftKeyboardMessenger {

	private final InputMethodManager inputMethodManager;

	public SoftKeyboardMessenger(Context context) {
		inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
	}

	public void showKeyboard(View view) {
		inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}

	public void hideKeyboard(Activity activity) {

		View view = activity.getCurrentFocus();

		if(view != null)
			inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
}
