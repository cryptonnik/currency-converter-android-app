package com.remaller.currencyconverter.ui;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.remaller.currencyconverter.ConverterApplication;
import com.remaller.currencyconverter.R;
import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.ui.utils.CurrencyIconLoader;
import com.remaller.currencyconverter.ui.utils.SoftKeyboardMessenger;

import java.util.List;

public class CurrencyConverterActivity
		extends AppCompatActivity
		implements CurrencyConverterActivityPresenter.PresenterView
{
	private CurrencyConverterActivityPresenter presenter;

	private SoftKeyboardMessenger softKeyboardMessenger;

	private RecyclerView recyclerView;
	private View errorMessageView;

	private RecyclerView.LayoutManager listLayoutManager;
	private CurrencyListAdapter listAdapter;

	private Snackbar serverErrorSnackbar;
	private Snackbar communicationErrorSnackbar;

	private boolean firstItemVisible;

	@Override
	public void onCreate(Bundle saveInstanceState) {

		presenter = ConverterApplication.getAppComponent().currencyConverterActivityPresenter();

		super.onCreate(saveInstanceState);
		setContentView(R.layout.activity_currency_converter);

		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		softKeyboardMessenger = new SoftKeyboardMessenger(this);

		recyclerView = findViewById(R.id.list);
		errorMessageView = findViewById(R.id.errorMessageView);

		recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 30);
		recyclerView.setItemViewCacheSize(30);
//		recyclerView.setItemAnimator(null);
		((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

		CurrencyIconLoader currencyIconLoader = new CurrencyIconLoader(this);
		listAdapter = new CurrencyListAdapter(softKeyboardMessenger, currencyIconLoader);
		recyclerView.setAdapter(listAdapter);

		listLayoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(listLayoutManager);

		listAdapter.setEventListener(new CurrencyListAdapter.EventListener() {
			@Override
			public void onItemClicked(String currency) {
				presenter.onItemClicked(currency);
			}

			@Override
			public void onFirstItemValueChanged(String value) {
				presenter.onFirstItemValueChanged(value);
			}
		});

		recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

			@Override
			public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {

				super.onDrawOver(c, parent, state);

				LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
				int pos = layoutManager.findFirstVisibleItemPosition();

				boolean firstItemVisibleNow = pos == 0;

				if(firstItemVisibleNow != firstItemVisible) {

					firstItemVisible = firstItemVisibleNow;
					presenter.onFirstItemVisibilityChanged(firstItemVisible);
				}
			}
		});

		presenter.onCreate(this);
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		presenter.onDestroy();
	}

	@Override
	public void onStart() {

		super.onStart();
		presenter.onStart();
	}

	@Override
	public void onStop() {

		super.onStop();
		presenter.onStop();
	}

	@Override
	public void onPause() {

		super.onPause();
		presenter.onPause();
	}

	// =============================================================================================

	@Override
	public void showNoRatesViewState() {

		recyclerView.setVisibility(View.GONE);
		errorMessageView.setVisibility(View.VISIBLE);
	}

	@Override
	public void setCurrencyValues(List<CurrencyListAdapter.Item> items, CurrencyRatesState.Error error) {

		recyclerView.setVisibility(View.VISIBLE);
		errorMessageView.setVisibility(View.GONE);

		listAdapter.setItems(items);

		if(error == null) {

			if(serverErrorSnackbar != null && serverErrorSnackbar.isShown())
				serverErrorSnackbar.dismiss();

			if(communicationErrorSnackbar != null && communicationErrorSnackbar.isShown())
				communicationErrorSnackbar.dismiss();

		} else if(error == CurrencyRatesState.Error.IncorrectServerResponse ||
				error == CurrencyRatesState.Error.IncorrectResponseCode) {

			if (serverErrorSnackbar == null) {

				View rootView = findViewById(R.id.root);
				serverErrorSnackbar = Snackbar.make(rootView, R.string.text_serverErrorMessage, Snackbar.LENGTH_INDEFINITE);
			}

			if(!serverErrorSnackbar.isShown())
				serverErrorSnackbar.show();

		} else {

			if (communicationErrorSnackbar == null) {

				View rootView = findViewById(R.id.root);
				communicationErrorSnackbar = Snackbar.make(rootView, R.string.text_communicationErrorMessage, Snackbar.LENGTH_INDEFINITE);
			}

			if(!communicationErrorSnackbar.isShown())
				communicationErrorSnackbar.show();
		}
	}

	@Override
	public void moveItemToTop(int itemPosition) {

		listAdapter.moveItemToTop(itemPosition);
		listLayoutManager.scrollToPosition(0);
	}

	@Override
	public void focusFirstItem() {
		listAdapter.focusFirstItem();
	}

	@Override
	public void hideKeyboard() {

		softKeyboardMessenger.hideKeyboard(CurrencyConverterActivity.this);
		listAdapter.removeFirstItemFocus();
	}
}
