package com.remaller.currencyconverter.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CurrencyOrderController {

	private List<String> currencyOrder = new LinkedList<>();

	public synchronized void setCurrencies(String baseCurrency, Collection<String> currencies) {

		List<String> newCurrencyOrder = new LinkedList<>(currencies);
		newCurrencyOrder.add(0, baseCurrency);

		if(currencyOrder.isEmpty())
			currencyOrder.add(baseCurrency);

		sortItems(newCurrencyOrder, currencyOrder);
		currencyOrder = newCurrencyOrder;
	}

	public synchronized void applyCurrencyOrder(List<String> newCurrencyOrder) {
		sortItems(currencyOrder, newCurrencyOrder);
	}

	public synchronized List<String> getCurrencyOrder() {
		return new ArrayList<>(currencyOrder);
	}

	public synchronized String getCurrencyByPos(int pos) {

		if(currencyOrder.isEmpty())
			return null;

		return currencyOrder.get(pos);
	}

	public synchronized int getCurrencyPos(String currency) {
		return currencyOrder.indexOf(currency);
	}

	public synchronized void moveCurrencyToTop(String currency) {

		if(currencyOrder.remove(currency))
			currencyOrder.add(0, currency);
	}

	// =============================================================================================

	private static void sortItems(List<String> items, List<String> newOrder) {

		Collections.sort(items, (o1, o2) -> {

			int index1 = newOrder.indexOf(o1);
			int index2 = newOrder.indexOf(o2);

			if(index1 != -1 && index2 != -1)
				return index1 < index2 ? -1 : 1;

			if(index1 != -1)
				return -1;

			if(index2 != -1)
				return 1;

			return o1.compareTo(o2);
		});
	}
}
