package com.remaller.currencyconverter.ui;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class CurrencyItemsBuilder {

	public List<CurrencyListAdapter.Item> createCurrencyItems(
			String topCurrency,
			Double topCurrencyAmountValue,
			String topCurrencyAmountString,
			List<String> currencyOrder,
			CurrencyAmountCalculator amountCalculator,
			CurrencyAmountFormatter amountFormatter
	) {
		List<CurrencyListAdapter.Item> items = new ArrayList<>(currencyOrder.size());

		for(String currency : currencyOrder) {

			CurrencyListAdapter.Item item = new CurrencyListAdapter.Item();

			Currency currencyObject = Currency.getInstance(currency);

			item.code = currency;

			if(topCurrency.equals(currency) && topCurrencyAmountString != null)
				item.amount = topCurrencyAmountString;
			else if(topCurrencyAmountValue == null)
				item.amount = null;
			else
				item.amount =
						amountFormatter.format(
								amountCalculator.calculateAmountInNewCurrency(topCurrencyAmountValue, topCurrency, currency),
								currencyObject);

			item.description = currencyObject.getDisplayName();

			items.add(item);
		}

		return items;
	}
}
