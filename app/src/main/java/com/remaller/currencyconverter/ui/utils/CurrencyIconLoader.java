package com.remaller.currencyconverter.ui.utils;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.remaller.currencyconverter.BuildConfig;
import com.remaller.currencyconverter.ui.glide_svg.SvgSoftwareLayerSetter;

public class CurrencyIconLoader {

	private final RequestBuilder requestBuilder;

	public CurrencyIconLoader(Context context) {

		requestBuilder = Glide.with(context)
				.as(PictureDrawable.class)
				.listener(new SvgSoftwareLayerSetter());
	}

	public void loadImage(String currency, ImageView imageView) {

		requestBuilder.load(String.format(BuildConfig.CURRENCY_FLAG_URL_PATTERN, currency.toLowerCase()))
				.into(imageView);
	}
}
