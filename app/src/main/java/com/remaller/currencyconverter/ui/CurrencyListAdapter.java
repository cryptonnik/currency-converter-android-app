package com.remaller.currencyconverter.ui;
;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.remaller.currencyconverter.R;
import com.remaller.currencyconverter.ui.utils.CurrencyIconLoader;
import com.remaller.currencyconverter.ui.utils.SoftKeyboardMessenger;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CurrencyListAdapter extends RecyclerView.Adapter<CurrencyListAdapter.ItemViewHolder> {

	public static class Item {

		public String code;
		public String description;
		public String amount;
	}

	public interface EventListener {

		void onItemClicked(String currency);
		void onFirstItemValueChanged(String value);
	}

	private final SoftKeyboardMessenger softKeyboardMessenger;
	private final List<Item> items = new ArrayList<>();

	private EventListener eventListener;

	private boolean showKeyboard;
	private boolean focusFirstItem;

	private final CurrencyIconLoader currencyIconLoader;

	public CurrencyListAdapter(
			SoftKeyboardMessenger softKeyboardMessenger,
			CurrencyIconLoader currencyIconLoader
	) {
		this.softKeyboardMessenger = softKeyboardMessenger;
		this.currencyIconLoader = currencyIconLoader;
	}

	public void setEventListener(EventListener eventListener) {
		this.eventListener = eventListener;
	}

	public void moveItemToTop(int pos) {

		if(pos >= items.size())
			return;

		Item item = items.get(pos);
		items.remove(pos);
		items.add(0, item);

		notifyItemMoved(pos, 0);
	}

	public void focusFirstItem() {

		if(showKeyboard)
			return;

		showKeyboard = true;
		focusFirstItem = true;
		notifyItemChanged(0);
	}

	public void removeFirstItemFocus() {

		if(!focusFirstItem)
			return;

		showKeyboard = false;
		focusFirstItem = false;
		notifyItemChanged(0);
	}

	public void setItems(List<Item> items) {

		if(items.size() == this.items.size())
			notifyItemRangeChanged(0, items.size());
		else
			notifyDataSetChanged();

		this.items.clear();
		this.items.addAll(items);
	}

	// =============================================================================================

	@Override
	public long getItemId(int position) {
		return items.get(position).code.hashCode();
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	@NonNull
	@Override
	public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

		LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
		View view = layoutInflater.inflate(R.layout.item_currency, viewGroup, false);

		ItemViewHolder itemViewHolder = new ItemViewHolder(view);

		DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
		DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
		itemViewHolder.amount.setKeyListener(DigitsKeyListener.getInstance("0123456789" + symbols.getDecimalSeparator()));

		return itemViewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int pos) {

		Item item = items.get(pos);

		itemViewHolder.code.setText(item.code);
		itemViewHolder.description.setText(item.description);

		String amountTextValue = item.amount;

		if(amountTextValue == null)
			amountTextValue = "";

		itemViewHolder.amount.removeTextChangedListener(textWatcher);
		itemViewHolder.amount.setOnFocusChangeListener(null);

		itemViewHolder.amount.setFocusable(pos == 0);
		itemViewHolder.amount.setFocusableInTouchMode(pos == 0);

		if(pos == 0) {

			int maxLength = Math.max(14, amountTextValue.length());

			itemViewHolder.amount.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

			if(!amountTextValue.equals(itemViewHolder.amount.getText().toString())) {

				itemViewHolder.amount.setText(amountTextValue);
			}

			if(focusFirstItem)
				itemViewHolder.amount.requestFocus();
			else
				itemViewHolder.amount.clearFocus();

			if(showKeyboard) {

				itemViewHolder.amount.setSelection(amountTextValue.length());
				softKeyboardMessenger.showKeyboard(itemViewHolder.amount);
				showKeyboard = false;
			}

			itemViewHolder.amount.setOnClickListener(null);

			itemViewHolder.amount.setOnFocusChangeListener((v, hasFocus) -> {
				if(hasFocus)
					eventListener.onItemClicked(item.code);
			});

			itemViewHolder.amount.addTextChangedListener(textWatcher);

		} else {

			itemViewHolder.amount.setFilters(new InputFilter[0]);
			itemViewHolder.amount.setText(amountTextValue);

			itemViewHolder.amount.setOnClickListener(v -> {
				eventListener.onItemClicked(item.code);
			});
		}

		itemViewHolder.itemView.setOnClickListener(v -> {
			eventListener.onItemClicked(item.code);
		});

		currencyIconLoader.loadImage(item.code, itemViewHolder.icon);
	}

	// =============================================================================================

	private final TextWatcher textWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

			EventListener eventListener = CurrencyListAdapter.this.eventListener;

			if(eventListener != null)
				eventListener.onFirstItemValueChanged(s.toString());
		}

		@Override
		public void afterTextChanged(Editable s) { }
	};

	class ItemViewHolder extends RecyclerView.ViewHolder {

		final ImageView icon;
		final TextView code;
		final TextView description;
		final EditText amount;

		ItemViewHolder(@NonNull View itemView) {

			super(itemView);

			icon = itemView.findViewById(R.id.icon);
			code = itemView.findViewById(R.id.currencyCode);
			description = itemView.findViewById(R.id.currencyDescription);
			amount = itemView.findViewById(R.id.amount);
		}
	}
}
