package com.remaller.currencyconverter.ui;

import java.util.HashMap;
import java.util.Map;

public class CurrencyAmountCalculator {

	private Map<String, Double> rates;
	private String ratesBaseCurrency;

	public synchronized void updateRates(String baseCurrency, Map<String, Double> ratesWithoutBase) {

		this.ratesBaseCurrency = baseCurrency;
		this.rates = new HashMap<>(ratesWithoutBase);
		this.rates.put(ratesBaseCurrency, 1.0);
	}

	public synchronized Double calculateAmountInNewCurrency(
			double amountInBaseCurrency,
			String baseCurrency,
			String newCurrency
	) {
		Double rate = rates.get(newCurrency);
		Double baseRate = rates.get(baseCurrency);

		if(baseRate == null || rate == null)
			return null;

		return amountInBaseCurrency * rate / baseRate;
	}
}
