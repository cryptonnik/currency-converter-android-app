package com.remaller.currencyconverter.service;

import org.joda.time.DateTime;

import java.util.Map;

public class CurrencyRatesState {

	public enum Error {
		IncorrectResponseCode,
		IncorrectServerResponse,
		Another
	}

	public final DateTime updateTime;
	public final String baseCurrency;
	public final Map<String, Double> rates;
	public final Error error;

	public CurrencyRatesState(
			DateTime updateTime,
			String baseCurrency,
			Map<String, Double> rates,
			Error error
	) {
		this.updateTime = updateTime;
		this.baseCurrency = baseCurrency;
		this.rates = rates;
		this.error = error;
	}

	public static CurrencyRatesState createEmptyState() {
		return new CurrencyRatesState(null, null, null, null);
	}

	public static CurrencyRatesState createState(
			DateTime updateTime,
			String baseCurrency,
			Map<String, Double> rates
	) {
		return new CurrencyRatesState(
				updateTime,
				baseCurrency,
				rates,
				null);
	}

	public static CurrencyRatesState createState(
			CurrencyRatesState oldState,
			Error error
	) {
		return new CurrencyRatesState(
				oldState.updateTime,
				oldState.baseCurrency,
				oldState.rates,
				error);
	}
}
