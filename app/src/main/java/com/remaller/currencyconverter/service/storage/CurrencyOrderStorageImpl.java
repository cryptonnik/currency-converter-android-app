package com.remaller.currencyconverter.service.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.remaller.currencyconverter.service.CurrencyOrderState;

import java.lang.reflect.Type;
import java.util.List;

public class CurrencyOrderStorageImpl implements CurrencyOrderStorage {

	private final static String KEY_ORDER = "order";
	private final static String KEY_FIRST_AMOUNT = "firstAmount";

	private final SharedPreferences preferences;
	private final Gson gson;

	public CurrencyOrderStorageImpl(Context context) {

		preferences = context.getSharedPreferences("currency_order", Context.MODE_PRIVATE);

		gson = new GsonBuilder()
				.create();
	}

	@Override
	public void saveOrderAndTopValue(List<String> order, float topValue) {

		String orderJson = gson.toJson(order);

		preferences.edit()
				.putString(KEY_ORDER, orderJson)
				.putFloat(KEY_FIRST_AMOUNT, topValue)
				.apply();
	}

	@Override
	public void saveTopValue(float topValue) {

		preferences.edit()
				.putFloat(KEY_FIRST_AMOUNT, topValue)
				.apply();
	}

	@Override
	public CurrencyOrderState loadOrderState() {

		String orderJson = preferences.getString(KEY_ORDER, null);
		float firstAmount = preferences.getFloat(KEY_FIRST_AMOUNT, 0.0f);

		if(orderJson != null) {

			Type listType = new TypeToken<List<String>>() {}.getType();

			CurrencyOrderState state =
					new CurrencyOrderState(
							gson.fromJson(orderJson, listType),
							firstAmount);

			return state;
		}

		return null;
	}
}
