package com.remaller.currencyconverter.service.storage;

import com.remaller.currencyconverter.service.CurrencyRatesState;

import org.joda.time.DateTime;

import java.util.Map;

public interface CurrencyRatesStorage {

	void saveRates(DateTime updateTime, String baseCurrency, Map<String, Double> rates);
	CurrencyRatesState loadRateState();
}
