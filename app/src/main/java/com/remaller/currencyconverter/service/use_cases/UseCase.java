package com.remaller.currencyconverter.service.use_cases;

import io.reactivex.Observable;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

public abstract class UseCase<T> {

	private ReplaySubject<T> subject = ReplaySubject.create();

	public final void execute() {

		try {
			perform(subject);
			subject.onComplete();

		} catch (Exception e) {
			processException(e);
			subject.onError(e);
		}
	}

	public Observable<T> getObservable() {
		return subject;
	}

	protected abstract void perform(Subject<T> subject) throws Exception;
	protected void processException(Exception e) { }
}
