package com.remaller.currencyconverter.service;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Map;

import io.reactivex.Observable;

public interface CurrencyRatesStateController {

	@NonNull
	CurrencyRatesState getRatesState();
	Observable<CurrencyRatesState> getRatesStateChangedObservable();

	void setRates(DateTime updateTime, String baseCurrency, Map<String, Double> rates);
	void setError(CurrencyRatesState.Error error);
}
