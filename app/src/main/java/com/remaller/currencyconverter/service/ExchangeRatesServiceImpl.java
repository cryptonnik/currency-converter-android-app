package com.remaller.currencyconverter.service;

import com.remaller.currencyconverter.service.use_cases.UseCase;
import com.remaller.currencyconverter.service.use_cases.UseCaseBuilder;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;

public class ExchangeRatesServiceImpl implements ExchangeRatesService {

	private final UseCaseBuilder useCaseBuilder;
	private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

	private boolean initialized;
	private ScheduledFuture scheduledFuture;

	public ExchangeRatesServiceImpl(
			UseCaseBuilder useCaseBuilder
	) {
		this.useCaseBuilder = useCaseBuilder;
	}

	@Override
	public synchronized void init() {

		if(initialized)
			return;

		initialized = true;

		executorService.execute(() -> {
			useCaseBuilder.createLoadCurrencyStateUseCase().execute();
		});
	}

	@Override
	public synchronized void startUpdating() {

		if(scheduledFuture != null)
			return;

		scheduledFuture = executorService.scheduleWithFixedDelay(() -> {
			useCaseBuilder.createUpdateRatesUseCase().execute();
		}, 0, 2, TimeUnit.SECONDS);
	}

	@Override
	public synchronized void stopUpdating() {

		if(scheduledFuture != null) {

			scheduledFuture.cancel(true);
			scheduledFuture = null;
		}
	}

	@Override
	public Single<CurrencyOrderState> loadCurrencyOrderState() {

		UseCase<CurrencyOrderState> useCase = useCaseBuilder.createLoadOrderStateUseCase();

		executorService.execute(() -> {
			useCase.execute();
		});

		return useCase.getObservable().firstOrError();
	}

	@Override
	public void saveCurrencyOrderState(List<String> order, float baseValue) {

		executorService.execute(() -> {
			useCaseBuilder.createSaveOrderUseCase(order, baseValue).execute();
		});
	}
}
