package com.remaller.currencyconverter.service;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class CurrencyRatesStateControllerImpl implements CurrencyRatesStateController {

	private final Subject<CurrencyRatesState> ratesSubject = PublishSubject.create();
	private CurrencyRatesState ratesState;

	public CurrencyRatesStateControllerImpl() {
		ratesState = CurrencyRatesState.createEmptyState();
	}

	@Override
	@NonNull
	public CurrencyRatesState getRatesState() {
		return ratesState;
	}

	@Override
	public Observable<CurrencyRatesState> getRatesStateChangedObservable() {
		return ratesSubject;
	}

	@Override
	public synchronized void setRates(DateTime updateTime, String baseCurrency, Map<String, Double> rates) {

		CurrencyRatesState newState =
				CurrencyRatesState.createState(
						updateTime,
						baseCurrency,
						rates);

		ratesState = newState;
		ratesSubject.onNext(newState);
	}

	@Override
	public synchronized void setError(CurrencyRatesState.Error error) {

		CurrencyRatesState newState = CurrencyRatesState.createState(ratesState, error);

		ratesState = newState;
		ratesSubject.onNext(newState);
	}
}
