package com.remaller.currencyconverter.service.use_cases;

import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorage;

import io.reactivex.subjects.Subject;

public class LoadRatesStateUseCase extends UseCase {

	private final CurrencyRatesStateController currencyRatesStateController;
	private final CurrencyRatesStorage ratesStorage;

	public LoadRatesStateUseCase(
			CurrencyRatesStateController currencyRatesStateController,
			CurrencyRatesStorage ratesStorage
	) {
		this.currencyRatesStateController = currencyRatesStateController;
		this.ratesStorage = ratesStorage;
	}

	@Override
	protected void perform(Subject subject) {

		CurrencyRatesState ratesState = ratesStorage.loadRateState();

		if(ratesState != null) {

			currencyRatesStateController.setRates(
					ratesState.updateTime, ratesState.baseCurrency, ratesState.rates);
		}
	}
}
