package com.remaller.currencyconverter.service.use_cases;

import com.remaller.currencyconverter.endpoint.ExchangeRatesEndpoint;
import com.remaller.currencyconverter.service.CurrencyOrderState;
import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.storage.CurrencyOrderStorage;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorage;

import java.util.List;

public class UseCaseBuilder {

	private final CurrencyRatesStateController currencyRatesStateController;
	private final CurrencyOrderStorage currencyOrderStorage;
	private final CurrencyRatesStorage currencyRatesStorage;
	private final ExchangeRatesEndpoint exchangeRatesEndpoint;

	public UseCaseBuilder(
			CurrencyRatesStateController currencyRatesStateController,
			CurrencyOrderStorage currencyOrderStorage,
			CurrencyRatesStorage currencyRatesStorage,
			ExchangeRatesEndpoint exchangeRatesEndpoint
	) {
		this.currencyRatesStateController = currencyRatesStateController;
		this.currencyOrderStorage = currencyOrderStorage;
		this.currencyRatesStorage = currencyRatesStorage;
		this.exchangeRatesEndpoint = exchangeRatesEndpoint;
	}

	public UseCase createLoadCurrencyStateUseCase() {

		return new LoadRatesStateUseCase(
				currencyRatesStateController,
				currencyRatesStorage);
	}

	public UseCase<CurrencyOrderState> createLoadOrderStateUseCase() {
		return new LoadOrderStateUseCase(currencyOrderStorage);
	}

	public UseCase createUpdateRatesUseCase() {

		return new UpdateRatesUseCase(
				currencyRatesStateController,
				exchangeRatesEndpoint,
				currencyRatesStorage);
	}

	public UseCase createSaveOrderUseCase(List<String> order, float topValue) {

		return new SaveOrderStateUseCase(
				order,
				topValue,
				currencyOrderStorage);
	}
}
