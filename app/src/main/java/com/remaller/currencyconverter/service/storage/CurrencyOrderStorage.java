package com.remaller.currencyconverter.service.storage;

import com.remaller.currencyconverter.service.CurrencyOrderState;

import java.util.List;

public interface CurrencyOrderStorage {

	void saveOrderAndTopValue(List<String> order, float topValue);
	void saveTopValue(float topValue);

	CurrencyOrderState loadOrderState();
}
