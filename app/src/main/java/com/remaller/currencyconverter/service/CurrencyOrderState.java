package com.remaller.currencyconverter.service;

import java.util.List;

public class CurrencyOrderState {

	public final List<String> order;
	public final float firstAmount;

	public CurrencyOrderState( List<String> order, float firstAmount) {

		this.order = order;
		this.firstAmount = firstAmount;
	}
}
