package com.remaller.currencyconverter.service.use_cases;

import com.remaller.currencyconverter.endpoint.ExchangeRatesEndpoint;
import com.remaller.currencyconverter.endpoint.GetRatesResponseModel;
import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorage;

import org.joda.time.DateTime;

import java.util.Locale;
import java.util.Map;

import io.reactivex.subjects.Subject;
import retrofit2.Call;
import retrofit2.Response;

public class UpdateRatesUseCase extends UseCase {

	private final CurrencyRatesStateController currencyRatesStateController;
	private final ExchangeRatesEndpoint exchangeRatesEndpoint;
	private final CurrencyRatesStorage ratesStorage;

	public UpdateRatesUseCase(
			CurrencyRatesStateController currencyRatesStateController,
			ExchangeRatesEndpoint exchangeRatesEndpoint,
			CurrencyRatesStorage ratesStorage
	) {
		this.currencyRatesStateController = currencyRatesStateController;
		this.exchangeRatesEndpoint = exchangeRatesEndpoint;
		this.ratesStorage = ratesStorage;
	}

	@Override
	protected void perform(Subject subject) throws Exception {

		Call<GetRatesResponseModel> call = exchangeRatesEndpoint.getRates("EUR");
		Response<GetRatesResponseModel> response = call.execute();

		if(response.code() != 200)
			throw new IncorrectResponseCodeException(response.code());

		DateTime updateTime = new DateTime();
		String baseCurrency = response.body().base;
		Map<String, Double> rates = response.body().rates;

		if(baseCurrency == null || rates == null)
			throw new IncorrectServerResponseException();

		ratesStorage.saveRates(
				updateTime,
				response.body().base,
				response.body().rates);

		currencyRatesStateController.setRates(
				updateTime,
				response.body().base,
				response.body().rates);
	}

	@Override
	protected void processException(Exception e) {

		if(e instanceof IncorrectResponseCodeException)
			currencyRatesStateController.setError(CurrencyRatesState.Error.IncorrectResponseCode);
		else if(e instanceof IncorrectServerResponseException)
			currencyRatesStateController.setError(CurrencyRatesState.Error.IncorrectServerResponse);
		else
			currencyRatesStateController.setError(CurrencyRatesState.Error.Another);
	}

	private class IncorrectResponseCodeException extends Exception {

		IncorrectResponseCodeException(int responseCode) {
			super(String.format(Locale.getDefault(),"Incorrect response code: %d", responseCode));
		}
	}

	private class IncorrectServerResponseException extends Exception {

		IncorrectServerResponseException() {
			super("Incorrect server response");
		}
	}
}
