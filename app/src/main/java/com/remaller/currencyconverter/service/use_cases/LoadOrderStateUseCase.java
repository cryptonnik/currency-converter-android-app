package com.remaller.currencyconverter.service.use_cases;

import com.remaller.currencyconverter.service.CurrencyOrderState;
import com.remaller.currencyconverter.service.storage.CurrencyOrderStorage;

import io.reactivex.subjects.Subject;

public class LoadOrderStateUseCase extends UseCase<CurrencyOrderState> {

	private final CurrencyOrderStorage orderStorage;

	public LoadOrderStateUseCase(
			CurrencyOrderStorage orderStorage
	) {
		this.orderStorage = orderStorage;
	}

	@Override
	protected void perform(Subject<CurrencyOrderState> subject) {

		CurrencyOrderState result = orderStorage.loadOrderState();
		subject.onNext(result);
	}
}
