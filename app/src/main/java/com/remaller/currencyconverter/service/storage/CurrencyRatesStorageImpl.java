package com.remaller.currencyconverter.service.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Keep;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.service.DateTimeGsonTypeAdapter;

import org.joda.time.DateTime;

import java.util.Map;

public class CurrencyRatesStorageImpl implements CurrencyRatesStorage {

	private final static String KEY_RATES = "rates";

	private final SharedPreferences preferences;
	private final Gson gson;

	public CurrencyRatesStorageImpl(Context context) {

		preferences = context.getSharedPreferences("currency_rates", Context.MODE_PRIVATE);

		gson = new GsonBuilder()
				.registerTypeAdapter(DateTime.class, new DateTimeGsonTypeAdapter())
				.create();
	}

	@Override
	public void saveRates(DateTime updateTime, String baseCurrency, Map<String, Double> rates) {

		RatesState ratesState = new RatesState();

		ratesState.updateTime = updateTime;
		ratesState.baseCurrency = baseCurrency;
		ratesState.rates = rates;

		String stateString = gson.toJson(ratesState);
		preferences.edit().putString(KEY_RATES, stateString).commit();
	}

	@Override
	public CurrencyRatesState loadRateState() {

		String ratesJson = preferences.getString(KEY_RATES, null);

		if(ratesJson != null) {

			RatesState ratesState = gson.fromJson(ratesJson, RatesState.class);

			CurrencyRatesState state =
					CurrencyRatesState.createState(
							ratesState.updateTime,
							ratesState.baseCurrency,
							ratesState.rates);

			return state;
		}

		return null;
	}

	@Keep
	private class RatesState {

		public DateTime updateTime;
		public String baseCurrency;
		public Map<String, Double> rates;
	}
}
