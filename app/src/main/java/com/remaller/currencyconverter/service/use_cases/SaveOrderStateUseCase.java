package com.remaller.currencyconverter.service.use_cases;

import com.remaller.currencyconverter.service.storage.CurrencyOrderStorage;

import java.util.List;

import io.reactivex.subjects.Subject;

public class SaveOrderStateUseCase extends UseCase {

	private final List<String> order;
	private final float topValue;
	private final CurrencyOrderStorage orderStorage;

	public SaveOrderStateUseCase(
			List<String> order,
			float topValue,
			CurrencyOrderStorage orderStorage
	) {
		this.topValue = topValue;
		this.order = order;
		this.orderStorage = orderStorage;
	}

	@Override
	protected void perform(Subject subject) {

		if(order != null)
			orderStorage.saveOrderAndTopValue(order, topValue);
		else
			orderStorage.saveTopValue(topValue);
	}
}
