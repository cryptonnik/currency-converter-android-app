package com.remaller.currencyconverter.service;

import java.util.List;

import io.reactivex.Single;

public interface ExchangeRatesService {

	void init();
	void startUpdating();
	void stopUpdating();

	Single<CurrencyOrderState> loadCurrencyOrderState();
	void saveCurrencyOrderState(List<String> order, float baseValue);
}
