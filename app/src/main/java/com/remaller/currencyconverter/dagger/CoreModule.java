package com.remaller.currencyconverter.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import com.remaller.currencyconverter.endpoint.ExchangeRatesEndpoint;
import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.CurrencyRatesStateControllerImpl;
import com.remaller.currencyconverter.service.ExchangeRatesService;
import com.remaller.currencyconverter.service.ExchangeRatesServiceImpl;
import com.remaller.currencyconverter.service.use_cases.UseCaseBuilder;
import com.remaller.currencyconverter.service.storage.CurrencyOrderStorage;
import com.remaller.currencyconverter.service.storage.CurrencyOrderStorageImpl;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorage;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorageImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CoreModule {

	@Provides
	@NonNull
	@Singleton
	public ExchangeRatesService provideExchangeRatesService(
			UseCaseBuilder useCaseBuilder
	) {
		return new ExchangeRatesServiceImpl(useCaseBuilder);
	}

	@Provides
	@NonNull
	@Singleton
	public CurrencyRatesStateController provideCurrencyStateController() {
		return new CurrencyRatesStateControllerImpl();
	}

	@Provides
	@NonNull
	@Singleton
	public UseCaseBuilder provideUseCaseBuilder(
			Context context,
			CurrencyRatesStateController currencyRatesStateController,
			ExchangeRatesEndpoint exchangeRatesEndpoint
	) {
		CurrencyOrderStorage currencyStateStorage = new CurrencyOrderStorageImpl(context);
		CurrencyRatesStorage currencyRatesStorage = new CurrencyRatesStorageImpl(context);

		return new UseCaseBuilder(
				currencyRatesStateController,
				currencyStateStorage,
				currencyRatesStorage,
				exchangeRatesEndpoint);
	}
}
