package com.remaller.currencyconverter.dagger;

import com.remaller.currencyconverter.service.ExchangeRatesService;
import com.remaller.currencyconverter.ui.CurrencyConverterActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {
		BaseModule.class,
		EndpointModule.class,
		CoreModule.class,
		PresenterModule.class
})
@Singleton
public interface AppComponent {

	CurrencyConverterActivityPresenter currencyConverterActivityPresenter();
	ExchangeRatesService exchangeRatesService();
}
