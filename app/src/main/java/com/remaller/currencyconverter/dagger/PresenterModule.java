package com.remaller.currencyconverter.dagger;

import android.support.annotation.NonNull;

import com.remaller.currencyconverter.service.CurrencyRatesStateController;
import com.remaller.currencyconverter.service.ExchangeRatesService;
import com.remaller.currencyconverter.ui.CurrencyAmountCalculator;
import com.remaller.currencyconverter.ui.CurrencyAmountFormatter;
import com.remaller.currencyconverter.ui.CurrencyConverterActivityPresenter;
import com.remaller.currencyconverter.ui.CurrencyItemsBuilder;
import com.remaller.currencyconverter.ui.CurrencyOrderController;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

	@Provides
	@NonNull
	public CurrencyConverterActivityPresenter provideCurrencyConverterActivityPresenter(
			ExchangeRatesService exchangeRatesService,
			CurrencyRatesStateController currencyRatesStateController
	) {
		return new CurrencyConverterActivityPresenter(
				currencyRatesStateController,
				exchangeRatesService,
				new CurrencyAmountCalculator(),
				new CurrencyAmountFormatter(),
				new CurrencyOrderController(),
				new CurrencyItemsBuilder());
	}
}
