package com.remaller.currencyconverter.dagger;

import android.support.annotation.NonNull;

import com.remaller.currencyconverter.endpoint.ExchangeRatesEndpoint;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class EndpointModule {

	@Provides
	@NonNull
	@Singleton
	public Retrofit provideRetrofit(OkHttpClient okHttpClient) {

		return new Retrofit.Builder()
				.baseUrl("https://revolut.duckdns.org/")
				.addConverterFactory(GsonConverterFactory.create())
				.client(okHttpClient)
				.build();
	}
	@Provides
	@NonNull
	@Singleton
	public ExchangeRatesEndpoint provideExchangeRatesEndpoint(Retrofit retrofit) {
		return retrofit.create(ExchangeRatesEndpoint.class);
	}

}
