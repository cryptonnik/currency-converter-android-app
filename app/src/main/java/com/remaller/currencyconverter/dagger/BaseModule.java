package com.remaller.currencyconverter.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class BaseModule {

	private final Context context;

	public BaseModule(Context context) {
		this.context = context;
	}

	@Provides
	@NonNull
	public Context provideContext() {
		return context;
	}

	@Provides
	@NonNull
	@Singleton
	public OkHttpClient provideOkHttpClient() {

		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
		loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		return new OkHttpClient.Builder()
				.readTimeout(10, TimeUnit.SECONDS)
				.writeTimeout(10, TimeUnit.SECONDS)
				.addInterceptor(loggingInterceptor)
				.build();
	}
}
