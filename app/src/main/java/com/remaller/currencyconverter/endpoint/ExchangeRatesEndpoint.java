package com.remaller.currencyconverter.endpoint;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ExchangeRatesEndpoint {

	@GET("latest")
	Call<GetRatesResponseModel> getRates(@Query("base") String base);
}
