package com.remaller.currencyconverter.endpoint;

import android.support.annotation.Keep;

import java.util.Map;

@Keep
public class GetRatesResponseModel {

	public String base;
	public String date;
	public Map<String, Double> rates;
}
