package com.remaller.currencyconverter;

import android.app.Application;

import com.remaller.currencyconverter.dagger.AppComponent;
import com.remaller.currencyconverter.dagger.BaseModule;
import com.remaller.currencyconverter.dagger.DaggerAppComponent;

import net.danlew.android.joda.JodaTimeAndroid;

public class ConverterApplication extends Application {

	private static AppComponent appComponent;

	public static AppComponent getAppComponent() {
		return appComponent;
	}

	@Override
	public void onCreate() {

		super.onCreate();

		JodaTimeAndroid.init(this);

		appComponent = DaggerAppComponent.builder()
				.baseModule(new BaseModule(this))
				.build();

		appComponent.exchangeRatesService().init();
	}
}
