package com.remaller.currencyconverter;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.remaller.currencyconverter.service.CurrencyRatesState;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorage;
import com.remaller.currencyconverter.service.storage.CurrencyRatesStorageImpl;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class RatesStorageTest {

	@Test
	public void useAppContext() {

		Context appContext = InstrumentationRegistry.getTargetContext();

		CurrencyRatesStorage ratesStorage = new CurrencyRatesStorageImpl(appContext);

		DateTime updateTime = new DateTime();
		String baseCurrency = "EUR";

		Map<String, Double> rates = new HashMap<>();
		rates.put("RUB", 72.0);
		rates.put("USD", 1.1000);

		ratesStorage.saveRates(updateTime, baseCurrency, rates);

		CurrencyRatesState loadedState = ratesStorage.loadRateState();

		assertEquals(loadedState.updateTime, updateTime);
		assertEquals(loadedState.baseCurrency, baseCurrency);
		assertEquals(loadedState.rates, rates);
	}
}
